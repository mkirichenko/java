package ru.nsu.mkirichenko.rs.task2;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;

/**
 * Created by mikhail on 13/04/2018, 00:27;
 */

public class XMLStreamReaderFilter extends StreamReaderDelegate implements AutoCloseable {

	public XMLStreamReaderFilter(XMLStreamReader reader) {
		super(reader);
	}

	@Override
	public String getNamespaceURI() {
		return "http://openstreetmap.org/osm/0.6";
	}
}
