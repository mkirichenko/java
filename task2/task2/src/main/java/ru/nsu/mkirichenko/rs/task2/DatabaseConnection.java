package ru.nsu.mkirichenko.rs.task2;

import org.postgresql.jdbc.PgConnection;
import org.postgresql.util.HostSpec;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Created by mikhail on 13/04/2018, 01:14;
 */

public class DatabaseConnection {

	private static Connection connection = null;

	private static void initialize() throws SQLException {
		if (connection != null) {
			return;
		}
		try {
			HostSpec hostSpec[] = new HostSpec[1];
			hostSpec[0] = new HostSpec("postgres", 5432);
			String user = "docker";
			String database = "docker";
			Properties properties = new Properties();
			properties.setProperty("password", "docker");
			String url = "postgres";
			connection = new PgConnection(hostSpec, user, database, properties, url);

			connection.setAutoCommit(false);
			Statement initialStatement = connection.createStatement();
			initialStatement.execute(
					"CREATE TABLE IF NOT EXISTS nodes (" +
							"id bigint primary key, " +
							"lat double precision, " +
							"lon vi, " +
							"usr text, " +
							"uid bigint, " +
							"visible boolean, " +
							"version bigint, " +
							"changeset bigint, " +
							"timestmp timestamp)"
			);
			initialStatement.execute("DELETE FROM nodes");
			connection.commit();
		} catch (SQLException e) {
			connection = null;
			throw e;
		}
	}

	private DatabaseConnection() {}

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			initialize();
		}
		return connection;
	}

	public static void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
