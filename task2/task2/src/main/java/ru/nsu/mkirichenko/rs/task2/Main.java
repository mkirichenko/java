package ru.nsu.mkirichenko.rs.task2;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.mkirichenko.rs.osm.Osm;
import ru.nsu.mkirichenko.rs.osm.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mikhail on 06/03/2018, 14:43;
 */

public class Main {

	public static void main(String args[]) {
		Logger logger = LoggerFactory.getLogger(Main.class);
		logger.info("OSM extractor");
		logger.info("Today is {}", new Date().toString());

		Nodes nodes;
		try {
			nodes = new Nodes();
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

		CompressorStreamFactory factory = new CompressorStreamFactory();
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

		JAXBContext context;
		Unmarshaller unmarshaller;
		try {
			context = JAXBContext.newInstance("ru.nsu.mkirichenko.rs.osm");
			unmarshaller = context.createUnmarshaller();
		} catch (JAXBException e) {
			e.printStackTrace();
			return;
		}


		try (FileInputStream fileInputStream = new FileInputStream(args[0]);
			 CompressorInputStream uncompressedStream = factory.createCompressorInputStream(CompressorStreamFactory.BZIP2, fileInputStream)) {
			try {
				XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(uncompressedStream);
				try (XMLStreamReaderFilter xmlReader = new XMLStreamReaderFilter(xmlStreamReader)) {
					Object root = unmarshaller.unmarshal(xmlReader);
					if (root instanceof Osm) {
						Osm osm = (Osm)root;
						List<Node> nodeList = osm.getNode();
						Iterator<Node> iterator = nodeList.iterator();
						Node node;
						long beginTime;
						long totalTime;

						/* Test 1 */
						beginTime = System.currentTimeMillis();
						for (int i = 0; i < 1000; ++i) {
							nodes.insertNodeSimple(iterator.next());
						}
						nodes.commitChanges();
						totalTime = (System.currentTimeMillis() - beginTime);
						System.out.println("Test 1: " + 1000.0 * 1000.0 / (double)totalTime);

						/* Test 2 */
						beginTime = System.currentTimeMillis();
						for (int i = 0; i < 1000; ++i) {
							nodes.insertNodePrepared(iterator.next());
						}
						nodes.commitChanges();
						totalTime = (System.currentTimeMillis() - beginTime);
						System.out.println("Test 2: " + 1000.0 * 1000.0 / (double)totalTime);

						/* Test 3 */
						beginTime = System.currentTimeMillis();
						for (int i = 0; i < 1000; ++i) {
							nodes.insertNodeAddBatch(iterator.next());
						}
						nodes.insertNodeExecuteBatch();
						nodes.commitChanges();
						totalTime = (System.currentTimeMillis() - beginTime);
						System.out.println("Test 3: " + 1000.0 * 1000.0 / (double)totalTime);

						/* Other nodes */
						int i = 0;
						while (iterator.hasNext()) {
							++i;
							nodes.insertNodeAddBatch(iterator.next());
							if (i == 10000) {
								i = 0;
								nodes.insertNodeExecuteBatch();
								nodes.commitChanges();
							}
						}
						if (i > 0) {
							nodes.insertNodeExecuteBatch();
							nodes.commitChanges();
						}
						System.out.println("All nodes were inserted");
					}
				} catch (SQLException | NullPointerException e) {
					e.printStackTrace();
				}
			} catch (XMLStreamException | JAXBException e) {
				e.printStackTrace();
			}
		} catch (IOException | CompressorException e) {
			e.printStackTrace();
		}

	}

}