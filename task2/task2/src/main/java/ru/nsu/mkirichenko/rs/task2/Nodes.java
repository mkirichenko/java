package ru.nsu.mkirichenko.rs.task2;

import ru.nsu.mkirichenko.rs.osm.Node;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;

/**
 * Created by mikhail on 13/04/2018, 01:34;
 */

public class Nodes {

	private Connection connection;

	private Statement statement;

	private PreparedStatement preparedStatement;

	private PreparedStatement batchStatement;

	private StringBuilder queryBuilder;

	public Nodes() throws SQLException {
		connection = DatabaseConnection.getConnection();
		statement = null;
		preparedStatement = null;
		batchStatement = null;
		queryBuilder = new StringBuilder(1024);
	}

	public void insertNodeSimple(Node node) throws SQLException {
		if (statement == null) {
			statement = connection.createStatement();
		}
		queryBuilder.setLength(0);
		queryBuilder.append("INSERT INTO nodes VALUES (");
		queryBuilder.append(node.getId()).append(',');
		queryBuilder.append(node.getLat()).append(',');
		queryBuilder.append(node.getLon()).append(',');
		queryBuilder.append('\'').append(node.getUser()).append('\'').append(',');
		queryBuilder.append(node.getUid()).append(',');
		queryBuilder.append(node.isVisible()).append(',');
		queryBuilder.append(node.getVersion()).append(',');
		queryBuilder.append(node.getChangeset()).append(',');
		queryBuilder.append('\'').append(new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis())).append('\'');
		queryBuilder.append(')');
		statement.execute(queryBuilder.toString());
	}

	public void insertNodeAndCommit(Node node) throws SQLException {
		insertNodePrepared(node);
		connection.commit();
	}

	public void insertNodePrepared(Node node) throws SQLException {
		if (preparedStatement == null) {
			preparedStatement = connection.prepareStatement("INSERT INTO nodes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		}
		preparedStatement.setLong(1, node.getId().longValue());
		preparedStatement.setDouble(2, node.getLat());
		preparedStatement.setDouble(3, node.getLon());
		preparedStatement.setString(4, node.getUser());
		preparedStatement.setLong(5, node.getUid().longValue());
		preparedStatement.setObject(6, node.isVisible(), Types.BOOLEAN);
		preparedStatement.setLong(7, node.getVersion().longValue());
		preparedStatement.setLong(8, node.getChangeset().longValue());
		preparedStatement.setTimestamp(9, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
		preparedStatement.execute();
	}

	public void insertNodeAddBatch(Node node) throws SQLException {
		if (batchStatement == null) {
			batchStatement = connection.prepareStatement("INSERT INTO nodes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		}
		batchStatement.setLong(1, node.getId().longValue());
		batchStatement.setDouble(2, node.getLat());
		batchStatement.setDouble(3, node.getLon());
		batchStatement.setString(4, node.getUser());
		batchStatement.setLong(5, node.getUid().longValue());
		batchStatement.setObject(6, node.isVisible(), Types.BOOLEAN);
		batchStatement.setLong(7, node.getVersion().longValue());
		batchStatement.setLong(8, node.getChangeset().longValue());
		batchStatement.setTimestamp(9, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
		batchStatement.addBatch();
	}

	public void insertNodeExecuteBatch() throws SQLException {
		if (batchStatement == null) {
			batchStatement = connection.prepareStatement("INSERT INTO nodes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		}
		batchStatement.executeBatch();
	}

	private void fillPreparedStatement(PreparedStatement statement, Node node) throws SQLException {
		statement.setLong(1, node.getId().longValue());
		statement.setDouble(2, node.getLat());
		statement.setDouble(3, node.getLon());
		statement.setString(4, node.getUser());
		statement.setLong(5, node.getUid().longValue());
		statement.setObject(6, node.isVisible(), Types.BOOLEAN);
		statement.setLong(7, node.getVersion().longValue());
		statement.setLong(8, node.getChangeset().longValue());
		statement.setTimestamp(9, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
	}

	public void commitChanges() throws SQLException {
		connection.commit();
	}

	public void updateNode(Node node) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("UPDATE nodes SET lat = ?, lon = ?, usr = ?, uid = ?, visible = ?, version = ?, changeset = ?, timestmp = ? WHERE id = ?");
		statement.setDouble(1, node.getLat());
		statement.setDouble(2, node.getLon());
		statement.setString(3, node.getUser());
		statement.setLong(4, node.getUid().longValue());
		statement.setObject(5, node.isVisible(), Types.BOOLEAN);
		statement.setLong(6, node.getVersion().longValue());
		statement.setLong(7, node.getChangeset().longValue());
		statement.setTimestamp(8, new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));
		statement.setLong(9, node.getId().longValue());
		statement.execute();
	}

	public void deleteNode(Node node) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("DELETE FROM nodes WHERE id = ?");
		statement.setLong(1, node.getId().longValue());
		statement.execute();
	}

	public Node getNode(BigInteger id) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT id, lat, lon, usr, uid, visible, version, changeset, timestmp FROM nodes WHERE id = ?");
		statement.setLong(1, id.longValue());
		ResultSet resultSet = statement.executeQuery();
		Node node = new Node();
		node.setId(BigInteger.valueOf(resultSet.getLong(1)));
		node.setLat(resultSet.getDouble(2));
		node.setLon(resultSet.getDouble(3));
		node.setUser(resultSet.getString(4));
		node.setUid(BigInteger.valueOf(resultSet.getLong(5)));
		node.setVisible(resultSet.getObject(6, Boolean.class));
		node.setVersion(BigInteger.valueOf(resultSet.getLong(7)));
		node.setChangeset(BigInteger.valueOf(resultSet.getLong(8)));
		try {
			XMLGregorianCalendar xmlTimestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			Timestamp timestamp = resultSet.getTimestamp(9);
			LocalDateTime localTime = timestamp.toLocalDateTime();
			xmlTimestamp.setYear(localTime.getYear());
			xmlTimestamp.setMonth(localTime.getMonthValue());
			xmlTimestamp.setDay(localTime.getDayOfMonth());
			xmlTimestamp.setHour(localTime.getHour());
			xmlTimestamp.setMinute(localTime.getMinute());
			xmlTimestamp.setSecond(localTime.getSecond());
			xmlTimestamp.setFractionalSecond(new BigDecimal("0." + localTime.getNano()));
			node.setTimestamp(xmlTimestamp);
		} catch (DatatypeConfigurationException e) {
			node.setTimestamp(null);
		}
		return node;
	}


}
