package ru.nsu.mkirichenko.rs.task3.model;

import org.postgresql.util.HStoreConverter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Map;

/**
 * Created by mikhail on 16/04/2018, 18:36;
 */

@Converter(autoApply = true)
public class TagConverter implements AttributeConverter<Map<String, String>, String> {

	@Override
	public String convertToDatabaseColumn(Map<String, String> map) {
		return HStoreConverter.toString(map);
	}

	@Override
	public Map<String, String> convertToEntityAttribute(String hstore) {
		return HStoreConverter.fromString(hstore);
	}
}
