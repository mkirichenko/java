package ru.nsu.mkirichenko.rs.task3;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.nsu.mkirichenko.rs.osm.Osm;
import ru.nsu.mkirichenko.rs.osm.Node;
import ru.nsu.mkirichenko.rs.osm.Relation;
import ru.nsu.mkirichenko.rs.osm.Way;
import ru.nsu.mkirichenko.rs.task3.model.NodeRepository;
import ru.nsu.mkirichenko.rs.task3.model.RelationRepository;
import ru.nsu.mkirichenko.rs.task3.model.WayRepository;
import ru.nsu.mkirichenko.rs.task3.utils.XMLStreamReaderFilter;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by mikhail on 06/03/2018, 14:43;
 */

@SpringBootApplication
@EnableJpaRepositories
@EnableWebMvc
public class Main implements CommandLineRunner {

	@Autowired
	DataSource dataSource;

	@Autowired
	NodeRepository nodeRepository;

	@Autowired
	WayRepository wayRepository;

	@Autowired
	RelationRepository relationRepository;

	public static void main(String args[]) {
		SpringApplication.run(Main.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Thread.sleep(1000);

		if (args.length > 0) {
			/* insertion */
			CompressorStreamFactory factory = new CompressorStreamFactory();
			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

			JAXBContext context;
			Unmarshaller unmarshaller;
			try {
				context = JAXBContext.newInstance("ru.nsu.mkirichenko.rs.osm");
				unmarshaller = context.createUnmarshaller();
			} catch (JAXBException e) {
				e.printStackTrace();
				return;
			}

			try (FileInputStream fileInputStream = new FileInputStream(args[0]);
				 CompressorInputStream uncompressedStream = factory.createCompressorInputStream(CompressorStreamFactory.BZIP2, fileInputStream)) {
				try {
					XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(uncompressedStream);
					try (XMLStreamReaderFilter xmlReader = new XMLStreamReaderFilter(xmlStreamReader)) {
						Object root = unmarshaller.unmarshal(xmlReader);
						if (root instanceof Osm) {
							Osm osm = (Osm)root;
							for (Node node : osm.getNode()) {
								nodeRepository.save(new ru.nsu.mkirichenko.rs.task3.model.Node(node));
							}
							for (Way way : osm.getWay()) {
								wayRepository.save(new ru.nsu.mkirichenko.rs.task3.model.Way(way));
							}
							for (Relation relation : osm.getRelation()) {
								relationRepository.save(new ru.nsu.mkirichenko.rs.task3.model.Relation(relation));
							}
							System.out.println("All objects were inserted");
						}
					} catch (JDBCException e) {
						e.printStackTrace();
					}
				} catch (XMLStreamException | JAXBException e) {
					e.printStackTrace();
				}
			} catch (IOException | CompressorException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Nodes by uid mbe17");
		List<ru.nsu.mkirichenko.rs.task3.model.Node> nodes = nodeRepository.findByUsr("mbe17");
		for (ru.nsu.mkirichenko.rs.task3.model.Node n : nodes) {
			System.out.println(n.getId());
		}
		System.out.println("End of nodes");


	}

}