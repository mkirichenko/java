package ru.nsu.mkirichenko.rs.task3.model;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.util.HStoreConverter;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mikhail on 16/04/2018, 23:58;
 */

// courtesy of: http://backtothefront.net/2011/storing-sets-keyvalue-pairs-single-db-column-hibernate-postgresql-hstore-type/
public class HStoreType implements UserType {

	public Object assemble(Serializable cached, Object owner)
			throws HibernateException {
		return cached;
	}

	public Object deepCopy(Object o) throws HibernateException {
		Map m = (Map) o;
		return new HashMap(m);
	}

	public Serializable disassemble(Object o) throws HibernateException {
		return (Serializable) o;
	}

	public boolean equals(Object o1, Object o2) throws HibernateException {
		Map m1 = (Map) o1;
		Map m2 = (Map) o2;
		return m1.equals(m2);
	}

	public int hashCode(Object o) throws HibernateException {
		return o.hashCode();
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
		String col = names[0];
		String val = rs.getString(col);
		return HStoreConverter.fromString(val);
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
		String s = HStoreConverter.toString((Map) value);
		st.setObject(index, s, Types.OTHER);
	}

	public boolean isMutable() {
		return true;
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	public Class returnedClass() {
		return Map.class;
	}

	public int[] sqlTypes() {
		return new int[] { Types.INTEGER };
	}
}