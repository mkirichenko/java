package ru.nsu.mkirichenko.rs.task3.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.nsu.mkirichenko.rs.osm.Tag;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mikhail on 16/04/2018, 17:11;
 */

@Entity
@Table(name = "nodes")
@TypeDef(name = "hstore", typeClass = HStoreType.class)
@NamedQuery(name = "Node.findWithinRadius", query = "SELECT n FROM Node n WHERE earth_distance(ll_to_earth(?1, ?2), ll_to_earth(n.lat, n.lon)) < ?3")
public class Node implements Serializable {

	@Id
	BigInteger id;
	Double lat;
	Double lon;
	@JsonProperty("user")
	String usr;
	BigInteger uid;
	Boolean visible;
	BigInteger version;
	BigInteger changeset;
	@JsonProperty("timestamp")
	Timestamp timestmp;

	@Type(type = "hstore")
	@Convert(disableConversion = true)
	Map<String, String> tags = new HashMap<>();

	public Node() {
	}

	public Node(ru.nsu.mkirichenko.rs.osm.Node node) {
		id = node.getId();
		lat = node.getLat();
		lon = node.getLon();
		usr = node.getUser();
		uid = node.getUid();
		visible = node.isVisible();
		version = node.getVersion();
		changeset = node.getChangeset();
		timestmp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());
		for (Tag tag : node.getTag()) {
			tags.put(tag.getK(), tag.getV());
		}
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public String getUsr() {
		return usr;
	}

	public void setUsr(String usr) {
		this.usr = usr;
	}

	public BigInteger getUid() {
		return uid;
	}

	public void setUid(BigInteger uid) {
		this.uid = uid;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public BigInteger getVersion() {
		return version;
	}

	public void setVersion(BigInteger version) {
		this.version = version;
	}

	public BigInteger getChangeset() {
		return changeset;
	}

	public void setChangeset(BigInteger changeset) {
		this.changeset = changeset;
	}

	public Timestamp getTimestmp() {
		return timestmp;
	}

	public void setTimestmp(Timestamp timestmp) {
		this.timestmp = timestmp;
	}

	public Map<String, String> getTags() {
		return tags;
	}

	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

}



