package ru.nsu.mkirichenko.rs.task3.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by mikhail on 17/04/2018, 15:55;
 */

@Repository
public interface RelationRepository extends JpaRepository<Relation, BigInteger> {

	List<Relation> findByUsr(String usr);

}
