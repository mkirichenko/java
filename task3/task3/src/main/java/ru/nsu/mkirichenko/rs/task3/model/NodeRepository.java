package ru.nsu.mkirichenko.rs.task3.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by mikhail on 17/04/2018, 01:18;
 */

@Repository
public interface NodeRepository extends JpaRepository<Node, BigInteger> {

	List<Node> findByUsr(String usr);

	List<Node> findWithinRadius(Double lat, Double lon, Double radius);

}
