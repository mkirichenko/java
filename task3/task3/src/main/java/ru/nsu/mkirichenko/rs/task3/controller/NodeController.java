package ru.nsu.mkirichenko.rs.task3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.mkirichenko.rs.task3.model.Node;
import ru.nsu.mkirichenko.rs.task3.model.NodeRepository;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Created by mikhail on 17/04/2018, 17:05;
 */

@RestController
public class NodeController {

	@Autowired
	NodeRepository nodeRepository;

	public NodeController() {

	}

	@RequestMapping("/getNode")
	public Node getNode(@RequestParam(value = "nid") BigInteger nid) {
		BigInteger nodeId;
		try {
			nodeId = nid;//new BigInteger(nid);
		} catch (NumberFormatException ne) {
			return null;
		}
		Optional<Node> optionalNode = nodeRepository.findById(nodeId);
		return optionalNode.orElse(null);
	}

	@RequestMapping("/getNodesByUser")
	public Node[] getNodesByUser(@RequestParam(value = "user") String user) {
		List<Node> nodeList = nodeRepository.findByUsr(user);
		Node nodes[] = new Node[nodeList.size()];
		return nodeList.toArray(nodes);
	}

	@RequestMapping("/removeNode")
	public Object removeNode(@RequestParam(value = "nid") String nid) {
		BigInteger nodeId;
		try {
			nodeId = new BigInteger(nid);
		} catch (NumberFormatException ne) {
			return ResponseEntity.badRequest().body(ne.getMessage());
		}
		nodeRepository.deleteById(nodeId);
		return true;
	}

	@RequestMapping("/updateNode")
	public Object updateNode(@RequestParam(value = "nid") BigInteger nid,
						   @RequestParam(value = "lat", required = false) Double lat,
						   @RequestParam(value = "lon", required = false) Double lon,
						   @RequestParam(value = "user", required = false) String user,
						   @RequestParam(value = "uid", required = false) BigInteger uid,
						   @RequestParam(value = "vis", required = false) Boolean visible,
						   @RequestParam(value = "ver", required = false) BigInteger version,
						   @RequestParam(value = "chs", required = false) BigInteger changeset,
						   @RequestParam(value = "time", required = false) Long time) {
		Node node = nodeRepository.findById(nid).orElse(null);
		if (node != null) {
			if (lat != null) {
				node.setLat(lat);
			}
			if (lon != null) {
				node.setLon(lon);
			}
			if (user != null) {
				node.setUsr(user);
			}
			if (uid != null) {
				node.setUid(uid);
			}
			if (visible != null) {
				node.setVisible(visible);
			}
			if (version != null) {
				node.setVersion(version);
			}
			if (changeset != null) {
				node.setChangeset(changeset);
			}
			if (time != null) {
				node.setTimestmp(new Timestamp(time));
			}
			nodeRepository.save(node);
		}
		return node;
	}

	//localhost:8080/getNodesWithinRadius?lat=55.0104369&lon=82.9169977&r=1000.0
	@RequestMapping("/getNodesWithinRadius")
	public Node[] getNodesWithinRadius(@RequestParam(value = "lat") Double lat,
									   @RequestParam(value = "lon") Double lon,
									   @RequestParam(value = "r", defaultValue = "1.0") Double radius) {
		List<Node> nodeList = nodeRepository.findWithinRadius(lat, lon, radius);
		Node nodes[] = new Node[nodeList.size()];
		return nodeList.toArray(nodes);
	}

}

