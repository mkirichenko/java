package ru.nsu.mkirichenko.rs.task3.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.nsu.mkirichenko.rs.osm.Tag;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mikhail on 16/04/2018, 17:11;
 */

@Entity
@Table(name = "relations")
@TypeDef(name = "hstore", typeClass = HStoreType.class)
public class Relation implements Serializable {

	@Id
	BigInteger id;
	@JsonProperty("user")
	String usr;
	BigInteger uid;
	Boolean visible;
	BigInteger version;
	BigInteger changeset;
	@JsonProperty("timestamp")
	Timestamp timestmp;


	@Type(type = "hstore")
	@Convert(disableConversion = true)
	Map<String, String> tags = new HashMap<>();

	public Relation() {
	}

	public Relation(ru.nsu.mkirichenko.rs.osm.Relation relation) {
		id = relation.getId();
		usr = relation.getUser();
		uid = relation.getUid();
		visible = relation.isVisible();
		version = relation.getVersion();
		changeset = relation.getChangeset();
		timestmp = new Timestamp(relation.getTimestamp().toGregorianCalendar().getTimeInMillis());
		for (Tag tag : relation.getTag()) {
			tags.put(tag.getK(), tag.getV());
		}
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUsr() {
		return usr;
	}

	public void setUsr(String usr) {
		this.usr = usr;
	}

	public BigInteger getUid() {
		return uid;
	}

	public void setUid(BigInteger uid) {
		this.uid = uid;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public BigInteger getVersion() {
		return version;
	}

	public void setVersion(BigInteger version) {
		this.version = version;
	}

	public BigInteger getChangeset() {
		return changeset;
	}

	public void setChangeset(BigInteger changeset) {
		this.changeset = changeset;
	}

	public Timestamp getTimestmp() {
		return timestmp;
	}

	public void setTimestmp(Timestamp timestmp) {
		this.timestmp = timestmp;
	}

	public Map<String, String> getTags() {
		return tags;
	}

	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

}
