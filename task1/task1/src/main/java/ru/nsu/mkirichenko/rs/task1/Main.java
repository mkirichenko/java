package ru.nsu.mkirichenko.rs.task1;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mikhail on 06/03/2018, 14:43;
 */

public class Main {

	public static void main(String args[]) {
		Logger logger = LoggerFactory.getLogger(Main.class);
		logger.info("Hello world!");
		logger.info("Now is {}", new Date().toString());

		CompressorStreamFactory factory = new CompressorStreamFactory();
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		HashMap<String, Integer> keyNodes = new HashMap<>();
		HashMap<String, Integer> userEdits = new HashMap<>();

		try (FileInputStream fileInputStream = new FileInputStream(args[0]);
			 CompressorInputStream uncompressedStream = factory.createCompressorInputStream(CompressorStreamFactory.BZIP2, fileInputStream)) {
			try {
				XMLStreamReader xmlReader = xmlInputFactory.createXMLStreamReader(uncompressedStream);
				int eventType;
				boolean isNodeElement = false;
				boolean isTagElement = false;
				String attributeName;
				String attributeValue;
				Integer previousValue;

				while (xmlReader.hasNext()) {
					eventType = xmlReader.next();
					switch (eventType) {
						case XMLStreamReader.START_ELEMENT:
							if (xmlReader.getLocalName().equals("node")) {
								isNodeElement = true;
							}
							if (isNodeElement && xmlReader.getLocalName().equals("tag")) {
								isTagElement = true;
							}
						case XMLStreamConstants.ATTRIBUTE:
							if (isNodeElement) {
								for (int i = 0; i < xmlReader.getAttributeCount(); ++i) {
									attributeName = xmlReader.getAttributeLocalName(i);
									if (!isTagElement && attributeName.equals("user")) {
										attributeValue = xmlReader.getAttributeValue(i);
										previousValue = userEdits.putIfAbsent(attributeValue, 1);
										if (previousValue != null) {
											userEdits.put(attributeValue, previousValue + 1);
										}
									}
									if (isTagElement && attributeName.equals("k")) {
										attributeValue = xmlReader.getAttributeValue(i);
										previousValue = keyNodes.putIfAbsent(attributeValue, 1);
										if (previousValue != null) {
											keyNodes.put(attributeValue, previousValue + 1);
										}
									}
								}
							}
							break;
						case XMLStreamConstants.END_ELEMENT:
							if (isNodeElement && xmlReader.getLocalName().equals("node")) {
								isNodeElement = false;
							}
							if (isTagElement && xmlReader.getLocalName().equals("tag")) {
								isTagElement = false;
							}
					}
				}
				xmlReader.close();
			} catch (XMLStreamException e) {
				logger.error(e.getMessage());
			}
		} catch (IOException | CompressorException e) {
			logger.error(e.getMessage());
		}

		System.out.println("Key - Nodes");
		sortAndPrintMap(keyNodes);

		System.out.println("\nUser - Edits");
		sortAndPrintMap(userEdits);
	}

	private static void sortAndPrintMap(HashMap<String, Integer> map) {
		ArrayList<Map.Entry<String, Integer>> entryList = new ArrayList<>(map.entrySet());
		entryList.stream().sorted(Comparator.comparing(Map.Entry::getValue)).forEach(e -> System.out.println(e.getKey() + ":" + e.getValue()));
	}
}

/*
Results:

INFO  | 2018-04-10 18:28:33.901 [main] ru.nsu.mkirichenko.rs.task1.Main - Hello world!
INFO  | 2018-04-10 18:28:33.922 [main] ru.nsu.mkirichenko.rs.task1.Main - Now is Tue Apr 10 18:28:33 NOVT 2018

Key - Nodes
subject:wikidata:1
anime:manga:1
toilets:wheelchair:1
source:url:1
service:vehicle:продажа_дисков:1
diet:vegan:1
payment:coins:1
service:vehicle:продажа_шин:1
harbour:1
seamark:topmark:shape:1
anime:video:1
seamark:topmark:colour:1
seamark:buoy_isolated_danger:colour:1
salt:1
h:1
service:bicycle:diy:1
ref:wmo:1
monitoring:weather:1
rooms:1
official_name1:1
female:1
info:1
name:vi:1
area:1
health_specialty:dentistry:1
alt_name:en:1
ref:en:1
note3:1
animal_boarding:1
service:tyres:1
anime:costume:1
self_service:1
surveillance:1
owner:1
abandoned:fuel:lpg:1
crane:type:1
room:1
payment:maestro:1
education:1
fuel:e85:1
surveillance:type:1
name:ce:1
social_facility:1
name:ar:1
seamark:harbour:category:1
fitness_station:1
generator:source:1
name:sty:1
name:cy:1
pump:type:1
name:da:1
url:1
smoothness:1
postal_code:1
name_int:1
capital:1
wikipedia_1:1
insurance:car:1
service:bicycle:retail:1
function:1
aerodrome:type:1
name:botanical:1
service:vehicle:brakes:1
fuel:HGV_diesel:1
service:vehicle:car_parts:1
service:vehicle:air_conditioning:1
xmas:feature:1
communication:radio:1
communication:television:1
abandoned:amenity:1
was:power:1
construction:1
was:fast_food:1
max_age:1
attribution:1
was:highway:1
advertising:1
name:kn:1
name:ko:1
seamark:buoy_cardinal:colour:1
seamark:buoy_isolated_danger:colour_pattern:1
emergency_service:1
alcohol:1
recording:manually:1
anime:gift:1
service:vehicle:car_repair:1
bridge:1
whitewater:1
male:1
abandoned:village:1
name_1:1
denotation:1
pump:1
name:lv:1
was:ref:1
country:1
goverment:1
service:vehicle:diagnostics:1
is_in:state:1
map_type:1
second_hand:1
name_official:1
name:fa:1
diameter_crown:1
addr:unit:1
support:1
fuel:e10:1
delivery:1
stars:1
was:operator:1
name:hu:1
admin_cеntre:1
addr:room:1
name:sr:1
bollard:1
dentist:for:1
utilization:1
indoor:1
colour:back:1
seasonal:1
old_ele:1
name:ur:1
toilets:position:1
name:tl:1
display:1
name:tt:1
seamark:buoy_cardinal:category:1
site:1
wetland:1
name:nn:1
map_size:1
repair:1
generator:output:electricity:1
car:1
cost:1
traffic_sign:forward:1
name:pt:1
service:vehicle:repairs:1
administrative:1
pedestrian:1
category:1
loc_name:en:2
artist_name:2
street_cabinet:2
service:vehicle:шиномонтаж:2
social_facility:for:2
ref:mkrf_museums:2
training:2
company:2
name:zh:2
allotments:2
was:name:2
payment:android_pay:2
communications_transponder:service:2
name:cs:2
note_2:2
name:eo:2
side:2
amenity_1:2
aerial:2
local_ref:2
admin_level:2
tower:2
bridge:support:2
beacon:type:2
service:vehicle:tyres:2
old_name:en:2
full_name:2
traffic_signals:sound:2
loc_name:ru:2
automated:2
survey:date:2
lit:2
name:fi:2
outdoor_seating:2
name:he:2
name:hi:2
min_age:2
name:sk:2
name:sl:2
name:sv:2
maxheight:2
incline:2
tram:2
name:ro:2
crossing:chicane:2
button_operated:2
payment:electronic_purses:2
hgv:2
payment:telephone_cards:2
start_date:2
old_name:ru:2
name:tr:2
depth:2
seamark:name:2
name:nl:2
name:no:2
tickets:avia:2
service:vehicle:oil_change:2
airmark:2
4wd_only:2
locality:3
fast_food:3
image:3
cash_in:3
bin:3
name:ca:3
kerb:3
name:et:3
name:es:3
trolleybus:3
payment:visa:3
payment:debit_cards:3
substation:3
vending:3
level_crossing:3
health_facility:type:3
payment:notes:3
cargo:3
crossing_ref:3
payment:mastercard:3
medical_system:western:3
access:citizenship:3
school:3
voltage:3
fire_hydrant:pressure:3
local_name:3
recycling:aluminium:3
tickets:train:3
recycling:plastic:4
payment:cash:4
unisex:4
seamark:beacon_special_purpose:category:4
tunnel:4
short_name:4
impromptu:4
source:population:4
platforms:4
addr:full:4
bing_offset:4
animal:4
wood:4
name:fr:4
drink:beer:4
recycling:glass_bottles:4
name:it:4
name:hr:4
recycling:scrap_metal:4
share_taxi:4
fire_hydrant:housenumber:4
healthcare:speciality:4
fire_hydrant:diameter:4
was:amenity:4
clothes:4
monitoring_station:5
gns:fc:5
loc_name:5
name:be:5
restriction:5
government:5
piste:type:5
tents:5
direction:5
openfire:5
payment:credit_cards:5
technology:5
icao:5
fire_operator:5
fax:5
diet:vegetarian:5
route_ref:5
lawyer:5
car_repair:6
drive_through:6
bing:offset:6
addr:province:6
fuel:octane_80:6
fire_hydrant:street:6
postcode:6
memorial:type:6
contact:instagram:7
park_ride:7
transformer:7
maxwidth:7
source:position:8
lift_gate:type:8
surface:8
bicycle_parking:8
addr:place:8
contact:ok:8
fire_hydrant:city:8
building_entrance:auto_open:8
maxspeed:9
tower:construction:9
gns:int_name:9
gns:ufi:9
gns:name:9
triple_tower:9
toilets:disposal:9
landuse:9
aerialway:9
gns:dsg:9
gauge:10
recycling:plastic_packaging:10
communication:mobile_phone:11
crossing:barrier:11
shelter_type:11
recycling:paper:12
recycling:plastic_bottles:12
recycling:clothes:12
esr:user:12
healthcare:12
code:12
traffic_signals:12
manhole:13
bic:13
transport:13
attraction:13
contact:youtube:13
frequency:13
fuel:cng:13
vehicle:13
inscription:13
station:13
todo:14
information:14
is_in:14
drinking_water:14
comment:14
traffic_sign:14
operator:wikidata:15
operator:wikipedia:15
smoking:15
water_well:15
water:15
crossing:bell:15
network:15
denomination:16
brand:wikipedia:16
brand:wikidata:16
recycling:cans:17
noexit:18
fire_hydrant:type:18
takeaway:18
recycling:glass:19
recycling_type:19
contact:twitter:20
seats:20
wifi:20
capacity:20
contact:email:21
layer:21
fuel:octane_92:21
contact:vk:22
crossing:light:22
waterway:22
oktmo:user:23
contact:facebook:23
internet_access:fee:24
religion:24
old_name:24
exit:25
dispensing:26
abandoned:26
aeroway:26
craft:27
parking:29
is_in:region:29
official_name:30
gns:modify_date:31
source:id:31
is_in:country_code:31
emergency:32
is_in:country:32
covered:32
traffic_signals:direction:33
name:pl:33
building:levels:34
service:36
admin_centre:37
name:de:37
artwork_type:37
substance:37
bunker_type:38
military:39
fee:39
name:lt:40
location:43
fuel:octane_98:43
waste:44
fuel:lpg:46
backrest:46
sport:47
note:50
height:55
tactile_paving:59
name:uk:63
abandoned:place:65
uic_ref:67
uic_name:67
pipeline:69
species:en:70
species:71
level:71
fuel:octane_91:71
motor_vehicle:75
internet_access:76
brand:77
horse:77
motorcycle:77
memorial:77
addr:subdistrict:80
supervised:81
note:warning:82
email:82
fuel:diesel:85
fuel:octane_95:87
int_name:90
int_ref:94
design:94
atm:97
fixme:103
alt_name:103
branch:107
type:116
bench:116
seamark:buoy_lateral:category:117
ele:122
source:ref:122
structure:125
office:133
name:ja:154
cuisine:155
seamark:buoy_lateral:colour:166
colour:168
motorcar:171
building:176
wheelchair:180
train:182
traffic_calming:195
description:205
official_status:206
seamark:buoy_lateral:system:213
seamark:type:221
okato:user:239
tower:type:241
contact:website:273
contact:phone:274
leisure:276
historic:291
phone:300
tourism:303
ford:315
access:342
addr:city:362
material:363
shelter:420
bus:510
website:516
name:ru:594
created_by:642
bicycle:674
addr:postcode:718
foot:751
addr:housenumber:873
addr:street:881
operator:1008
opening_hours:1133
man_made:1151
public_transport:1163
crossing:1393
addr:flats:1406
source:1448
name:en:1456
ref:1619
addr:district:1622
addr:region:1656
wikipedia:1718
wikidata:1720
population:date:1776
population:1777
addr:country:1793
place:2154
shop:2281
leaf_type:2385
leaf_cycle:2430
railway:2668
amenity:4145
barrier:4294
highway:4730
entrance:5410
natural:5587
name:9232
power:45140

User - Edits
iHedonist:1
EditAmateur:1
alexey_zakharenkov:1
Lussi3:1
Василий Малыгин:1
Andrey Tikhobaev:1
ForstEK:1
Ростислав Семенюк:1
aceman444:1
RovasT:1
Yuliya28:1
Centaur-x:1
xof:1
HiMan:1
Filipp Marinin:1
kasnsk:1
Yevgeny Gromov:1
Tropina Olesya:1
Modemoid:1
KonstantinDukemb:1
OpenVegeMap:1
Школа кос:1
zetx16:1
AlexZolotarev:1
vvk123:1
MrBrightsideTK:1
nyuriks:1
Malikjon:1
FSA:1
geodreieck4711:1
leva18:1
hdnsk:1
TAPAKAH:1
Mike Zubkov:1
siffash:1
Костя Кошкин:1
Владимир Гусятников:1
Nukl3OluS:1
Дведь:1
Caron Arthur:1
R0bst3r:1
torum077:1
metal kЫng:1
Людмила Малышева:1
Юрий Кузьменко:1
eugene3306:1
MR1kabanuch:1
int_ua:1
Row:1
Руслан Павлов:1
Seotime:1
kuzmatsukanov:1
NTKe:1
Alex Makaroff:1
spark-23:1
efred:1
Артем Громов:1
Станислав Лыжин:1
Gulevatov A:1
amalakh:1
Naty90:1
AT Studia:1
Игорь Малкиель:1
Андрей Бучельников:1
dmitr333:1
PavelBlossom:1
Алексей Чумаков:1
Peter Aksamirsky:1
NoelB:1
Aura Anyway:1
zsm991:1
plastun:1
PETROV Pavel:1
bby2009:1
Chilliysky:1
DJ Andre:1
cgu66:1
046:1
Myasanton:1
Osstapik:1
Дмитрий Неклюдов:1
Valery  Nikitin:1
Ivazin:1
Andy117:1
Minisuka:1
Священник Андрей Александрович Крашенинников:1
iRadysyuk:1
BurcBuckengolc:1
OlegIvanov:1
stewie_fisher:1
Kate Bordyug:1
kovr:1
Jonny80:1
Jickx:1
leg47:1
Артем Киян:1
GruppoAC1:1
Dashaaaaa:1
Drolll:1
Yury  Mogilin:1
Роман Черников:1
Stepan Serdyuk:1
vvoovv:1
Vazhnov Alexey:1
Андрей 1984:1
Dracon64:1
navigARTor:1
Vitozz90:1
hkm:1
yaric:1
uno3_2012:1
reta:1
Игорь Логачев:1
Azor:1
Volodia0164:1
asfintesco:1
Татьяна Аллилуева:1
Tatyana54:1
poksh:1
Dolyc:1
Pavel Konovalov:1
WonderPet47:1
Allow1977:1
Сергей Ляпко:1
ZhaziraZhumash:1
PapaHotelW:1
Спайси:1
Kradef:1
stenl:1
Nikita Girin:1
Oleg Tyumencev:1
Alexander Gusev:1
ЗZZZ:1
Arty Po:1
Stupnickiy:1
DEATHKISS PROSTO:1
Alexandr Ivanov:1
Ostap10:1
zond-z:1
jinalfoflia:1
Денис Денис:1
Сергей Налимов:1
Chekotryas:1
Ivan Zhuravlev:1
letchik:1
Robowolfer:1
Claudius Henrichs:1
Alexey Spiridonov:1
HolgerJeromin:1
Muravey:1
biun:1
kudryavsky:1
Grot-75:1
evgenykatyshev:1
van0:1
Asdfgf:1
AndiG88:1
Evgenyc:1
Анна Анина:1
sirorezka:1
Kali0stro:1
YAlexandra:1
Kassten:1
Sergey Dorokhin:1
Игорь Киселев61:1
Сергей Финд:1
zverevsergei:1
Vlasoft:1
IGNAT_VELIKIY:1
woodpeck:1
Дегтерёв Роман:1
GriFF:1
upendrakarukonda:1
uncrn:1
Fabi2:1
Classen:1
Cristian Arias:1
Born040590:1
viktr:1
nikolamedik:1
mbe57:1
k1pro:1
arhalex1985:1
greywind:1
Kick1978:1
Sirdolik:1
Rodrigo Rega:1
Ergo:1
Torsten_B:1
Yourez:1
andrewa78:1
Вероника93:1
Роман Шандов:1
Syl:1
Пегий:1
MIKHAIL_Dd:1
Saikol:1
Evgeniya002:1
Дмитрий Шустик:1
lexa36588:1
Dmitriy Neklyudov:1
Maxim Solodukha:1
Nik9391:1
Djonik:1
Сергей Здорово:1
Александр2205:1
Sherbak:1
Vorobyev Alexandr:1
Ásmóðr:1
aquamarishechka:1
D is Developer:1
nebm51:1
M1F:1
Dmitry  Rogov:1
b-olga-n:1
Amaroussi:1
Gutsycat:1
Ксения Лукомская:1
KekcuHa:1
Ropino:1
Surgeo:1
CupIvan:1
SolomahaZeexee:1
registraseo:1
tixuwuoz:1
sdliss:1
Юрий Кривогорницын:1
Alexander108:1
mikser33:1
dmitmia:1
Chingiz Batyrov:1
iMiKED:1
nebulon42:1
Kristinajdm:1
Ezhick:1
serge1982:1
K2Zed:1
Павел Рюмшин:1
Viacheslav Fatin:1
Твой портрет на заказ:1
Dron_007:1
Student1984:2
den0508:2
frol9:2
Rub21:2
vovchic:2
донор:2
Александр Александрович:2
apca:2
fomichenko:2
san_master:2
elena20112602:2
geozeisig:2
Oleg Tsviklinskiy:2
Alexandra_Livenets:2
Alexander Chirkov:2
PS V:2
Sergej Bestuzhev:2
Depsy:2
Alex_Kon:2
Георгий Р:2
Andrey-N:2
flohoff:2
fernan_ci:2
Мишин Григорий:2
maggot27:2
Alex939:2
1234glaz:2
Elena Marchenko:2
Sen-Я:2
tvm:2
Vladislav Naumov:2
вахтовик:2
гришка:2
Маша1993:2
Alex Astahov:2
Геннадий Дорохов:2
MaryVeryTheOne:2
Golova:2
Shipelkin:2
AlexTheTux:2
bull31:2
TanyaKaramysheva555:2
Zv-maxx:2
Valentin  Lutaev:2
mueschel:2
branishh:2
Алех:2
Женя Русаков:2
Anrock:2
Бурцев Максим:2
Sergey Astakhov:2
al56:2
Klinika PRETOR:2
Denisvonline:2
Runis:2
Tordanik:2
Sekt®:2
Ramzes:2
MaxIvanovich:2
kreont:2
Saveli:2
Myst:2
Lyubchenko Denis:2
Log87:2
king_nsk:2
Andre68:2
zed-nsk:2
Gorkppnsk:2
Bman:2
ken826:2
Veronika Dominguez:2
M I†I S:2
suuntta:2
Ksk91:2
RomanZar:2
Ivan Chelyubeev:2
mrmurrdoc:2
Max123456789max:2
ramyaragupathy:2
argrento:2
pogoreli:2
Vyvern:2
Steve_32:2
floscher:2
Traveler's Coffee:2
Alexey D:2
Saniga85:2
kolyubashka:2
Kolek86:2
mavl:2
Paspartu:2
bossleo:2
nafania:2
yaleks:2
prostomax:2
George PalmMuted:2
YurijZhirnov:2
Катерина*:3
Sargis-T111:3
karta54rus:3
Виктор Солдатченко:3
thetornado76:3
CutterRus:3
TSA21432:3
Sergejj-snt:3
a_isaeva:3
Nikolay Chernykh:3
jleh:3
ohmy:3
beweta:3
imbizo:3
ksenia:3
art2010:3
Евгений Кох:3
ДанилМ:3
Андрей Головин:3
b108:3
Nekrasovmy:3
Ермак:3
HJFGJHfg:3
Dr Kludge:3
pilat:3
OlegKey:3
highflyer74:3
sashazykov:3
Tim Morley:3
masha33:3
Mikhail Kalenkov:3
Григорий Ше:3
Bublikks:3
Ezovskikh:3
Vladimir Gin:3
Kira_Nsk:3
Aristo:3
Taxivs:3
maximuchnovosib:3
Stanislav Novosibirsk:3
IainMaclean:3
Kazikin:3
аркан:3
salik154:3
fris:3
Евгений zzz:3
zjohnes:3
GanKo:3
Dan4411:3
yasaniok:3
markoverride:3
Zverik:3
splatoonlover33:3
borodad:3
Долана:4
RAlexander:4
BoomEngine:4
alex427754:4
Artem_A:4
Aleks-Berlin:4
MZaitsev:4
vitalipet:4
ToGr:4
Vaska72:4
PA94:4
Alan Trick:4
Евгений Юркин:4
landfahrer:4
venomyd:4
De14ri10ng:4
Inok:4
MrBulldog:4
taeno:4
снежа:4
LeTopographeFou:4
Skomoroshka:4
dyadMisha:4
FrViPofm:4
КсенияЭБА:4
Gluk02:4
Stasiz:4
dedNikifor:4
BiIbo:4
Andrej Kolar:4
Lelik070:4
andygol:4
withoutface:4
Hilchenko Aleksey:4
armahema:4
Tigran75:4
pratikyadav:4
Отличница:4
BORZOTA:4
Андрей2106:4
KlaydF:4
Elena Sergeeva:4
Mahzii:4
Алексей Владимирович170478:4
first-leon:4
ShokReD:4
Роман Ларкин:4
ignat_b:4
shmuft:4
Евгенова Людмила:4
VeloNightGroup:4
wheelmap_visitor:4
~Grek~:4
Niker:4
sibklubok:4
kemmok:4
Kutuzov_optim:4
DIMM54ka:4
ivanilova:4
StasPekur:4
Kolek54:4
Сергей_Новосибирск:4
Zkir:5
iaros:5
gwinder:5
Pas:5
Detonator:5
kapellan_nsk:5
ruthmaben:5
Dm-svet:5
MartinTM:5
Dmitriy Sysoev:5
Sergey_Ivanovich:5
gadina:5
asspirine:5
MaximNikiforov:5
Александр Джипик:5
Barmagon:5
Miheich:5
kremlao:5
Storm-Sli:5
metslerff:5
Srbija_u_Rusiji:5
photokv:5
Katya817:5
Романенко София:5
xVir:5
Екатерина Пятова:5
omagic:5
av-nov:5
obstr:5
justhack:5
nki251:5
natanga:5
Дмитрий Владимирович Шестаков:5
k0rd:5
Денис1983:5
Sibirier:5
maryfedor:5
Hilton Hotels:5
LMNNsk:5
MadBorman:5
hadhuey:5
Аннэтик:5
DemanO:5
skip__:5
peshkov:5
n06rin:5
Jim_Di:5
Biller3d:6
void_nsk:6
dmOx:6
Boobies:6
Igoryok Usupov:6
Anton Buha:6
GorskyGriFF:6
nick_volynkin:6
ewgenii78:6
Nechesov:6
sanek01:6
ZCC:6
nevw:6
Sergei_ssn:6
AxiS:6
Евгения Плесовских:6
sadko1978m:6
woodpeck_repair:6
F-5 System:6
anyerdna:6
isaenko0745:6
avtobest:6
Сержант:6
сибиряк54:6
E K:6
jatisw:6
Expensev:6
Митёк:6
Alisha:6
Roman E:6
Ник С:6
Дмитрий1308:7
Sad54:7
Manu1400:7
pelmennumberone:7
Antony N:7
SVA:7
Sergey93:7
asinko03:7
atsman:7
Sirozha:7
halgrep:7
Orinocun:7
alexxanddr:7
Al6ka:7
Алексей Баранов:7
Oleg Tumtchouk:7
Stranderer:7
Juli-li:7
deksden:7
IlyaKhaustov:7
Маргарита:7
Vadim Ippolitov:7
serge_vl:8
PlasmaGAN:8
xTars:8
Игнат Аникеев:8
v22baev:8
aq123:8
OSMF Redaction Account:8
zqax:8
alexey_malgin:8
autodel:8
KopteDan:8
Володя71:8
Станислав Усышкин:8
2008vit:8
Mogamashina:8
chid93:8
Lebedev78:8
Евгений Михеев:8
Фурманов:8
drewy:8
Anton Dessiatov:8
Shonchalay:8
streetserpen:9
guest:9
Kanieva_Maria:9
Aleksandr Markin:9
iu2188:9
luiswoo:9
leonet:9
mike140:9
Серей24:9
VadimZ:9
Михаил Новиков:9
MakcuM_1984:9
AlexBychkov:9
w(HiT)e:9
KonoSV:9
Дмитрий Неизвестный:9
filu:9
xybot:9
Khan54rus:9
JKnight Jknight:9
XaE:9
NOOBerman:9
gamma-aspirin:9
Fortress:9
lks1:9
MockuT:10
Axill:10
Евгений Чертенков:10
154154154:10
Eugene Bogoyavlensky:10
demonlord32:10
fidoez:10
saikabhi:10
alexMAP:10
Striker2000:10
MaxS:10
baks72:10
GoodOk:10
novodim:10
develmax:10
artpavlov:10
freietonne-db:10
aos1986:10
Andrei  Polianski:10
Andrei Polushin:10
palunya:10
Андрей2013:10
katty91:10
Yuriy2101:10
IqRus:11
Шурка:11
bober953:11
birkoff:11
serj02:11
Opeckun:11
ILyaphp:11
toroboan:11
Лариса Насырова:11
theandyseitz:11
Alexey Sirotkin:11
Илья Усенко:11
k05ta:11
kupino-cck:12
samely_labuildings:12
Алина Рюмшина:12
FaveOfTheSun:12
GerdP:12
hazardm:12
ullus:12
Uzhur:12
Toxa-xa:12
Александр Питьев:12
mudozvon:12
ckopnuo-67:12
Evgeniy Rotanov:12
osm-kazakhstan:12
Just55:13
Алексей Аленич:13
Ordin:13
Iryna13:13
gabenok:13
KsushKA:13
Margarita24:13
karitotp:13
Peter Suvorov:13
stas nobody:13
supremevil:14
adima:14
elazarev:14
Елена123456789:14
yalfhg:14
VyacheslavS:14
Dmitry Torshin:14
Ольга26:14
mrDru:14
Andreevdp:15
Tom_Holland:15
andrewry:15
Andrey  Bek:15
SuperJet59rus:15
ittomsk:15
Karahagen:15
mad_sanity:15
SergeyNSK:16
Aleksey Okkel:16
Fopoehg Ghask Nmdj YOU OP HD - FullHD:16
Keks_Pexx:16
Luis36995:16
Kostya8792:16
Кирилл Казаков:16
solomax:16
fserges:16
ediyes:17
wildMan:17
Zhulik:17
vespex:17
Артём Шевчук:17
Bulldouzer:17
Mir76:17
Albert1974:17
yurasi_import:17
rain_99:17
TRD55:17
redopium:17
Wol1981:18
Nesmero:18
Yarroslav:18
IvanRigovsky:18
MBear:18
Exella:18
esperu:19
pyadov:19
Alexiofx:19
map-vl-ru:19
Kuritsyn Roman:20
Bessarab:20
Monitor:20
Micogen:20
Koljan87:20
GaM:20
Konstantin343:20
fernando_67:21
Denm4a1:21
medvezhut:22
Pavel1377:22
Dimokpower:22
rip:22
Shaun Austin:22
iDamir:22
rbuch703:22
Rockstar:22
камикадзе:22
Михаил Борисов:23
romanwhite_ru:23
Dmitry Olyenyov:23
Den26:23
_Martin2014_:23
rumigu:23
easmik:23
sadgin:23
Torshin:24
Freem:24
CatWhoCode:24
Andrey Chichak:25
CAHbKA-IV:25
Dimkansk:25
qwerty51:25
raf_gps:25
staytuned:25
Виктория:25
bodya09:25
x86128:25
piligab:25
Badani:26
Iskanchez:26
koala42:26
Дядя Алекс:26
Konstantin Svintsov:26
Lehansk90:26
svdeys:26
LittlePONNY:26
_DR_:27
calfarome:27
EugeneVD:27
kolen:27
Evgeniy 17174:27
snr:27
RamiresRaul:27
Arminar:27
Bedouin:28
vGross:28
Maxim1337:28
Aldorens:28
mk13:28
Danidin9:28
Юкатан:29
Андрей_SwEm:29
Alex Pole:29
Nikolaich:29
AGulliver:29
Galy4a:29
torovec:29
cvuneeez:30
Мехаил:30
DuhaNsk:30
WJtW:31
Alkozin:31
Romanso:31
ArnoldAleksandr:31
Strober:31
_govorilka:31
maria159:32
Vadim Zudkin:32
igornsk65:32
Den3309:32
Nikomuro:33
xSmit:33
Avlaak:33
Alexandra Pogudina:33
AlexDaisy:34
h4Cx78:34
JFK73:34
Алексей Трифоненко:34
_Makc:34
TourenHannes:34
eugennik:34
Alex94:35
Oleg  Kuzmin:35
mariya-romasheva:35
pilezkiy:35
gsv:36
Andrej-2:36
oldmen:36
pol78:36
LaY_zzz:37
Sub-Hero:37
T_G_K:37
IgorAS:38
svtol:38
Evgenii__K:38
zSilas:38
xmd5a:39
icepilot:39
Ensay:39
Akela710:40
turula:40
rusbuk:40
AndrewTheStalker:40
emptySA:40
AndRey42Rus:40
VTanya:41
Rimdus:41
arteam:41
Саныч23:41
zhenyayatsko:41
anton_sh:42
etajin:42
vomuanyd:42
RuslanRR:43
Фаруков:43
lurlrlrl:43
vanted:43
Art9:43
Макс:44
salmin:44
Paratruper:44
роман ильиных:44
purumub:45
alcm-b:45
unknown365:46
johny:46
svetolk:47
Vbif:47
sashkovishe:47
Skrat  nsk:48
lev656699:48
max630:48
Влад_chip:48
Adrewniy:49
gpspilot1:49
AntonSlusar:49
Pavel513:49
loktionov:49
S T:51
AlexeyRodionov:52
Vympel:52
Bloodmage:53
Jane4ka:53
usergray:54
Wild1:54
Pavlo Lee:55
Paul Shmarev:55
Erelen:56
Yar:56
Max Vasilev:56
Kudr:56
Алексей Дорохов:56
wd75:56
m3a1:56
alexderev:57
i-l-j-a:58
kotofos:59
ridixcr:59
fubor76:59
sibwest:59
Vladimir_R:59
Rage Sergey:61
Ismagilov Roman:62
xzmmmm:62
wowikt:62
KartaFan:63
Ян Козлаков:63
greencaps:64
pyram:64
Silent7:65
sfedor:65
Di Anre:65
bigalxyz123:67
arcth:68
Ansider:69
Donatello:70
vfjj:70
Kirasir:71
wawont:71
m5bere2:71
hsn208:71
Зайцева Алена:71
fd645555:71
AliasVector:74
LamerXaKer:75
3alei:76
Volchara:76
Nikolja Rybin:76
Felis Pimeja:77
Olta:77
ssv:77
GT21:77
Евген1991:78
Heinz_V:78
AlRight:78
Carbonio:78
Sergey001:78
agamemn:79
kamondir:79
Evgeny Vlasenko:79
axelr:80
yurasi:80
Skifarius:80
Dmanys:81
ThezKing:82
Evrozvuk:82
maxphizik:84
dostalker:84
VVD:86
abel801:87
Беломир:89
Андрей Омельчук:92
dedok:92
Alex_Ch01:92
gao:92
Холи:93
Demon_NSK:94
werner2101:94
kosmosoff:94
Voldemar0:95
Maarten Deen:97
DerLeere:97
xakpc:97
Иван-Сусанин:97
jaimemd:99
Alexey Baturin:100
altsky:100
wicked:100
Larchen:100
Citibiker:102
ivn64:102
tav:102
Chentum:103
Ruffian:105
Dizzyly:105
Mars_Bogdanow:106
agilalex:106
AduchiMergen:110
tony-k:110
Alexander Svidcehnkov:111
Helmchen42:112
soumrachnij:113
TarzanASG:113
ipp1963:115
webart50:118
Brom:118
ddams79:118
w23:118
Constantin1913:120
RichRico:121
mit3612:121
Вова Руднев:122
Practix:123
Ed_MSL:124
antonpa:125
RymenarNsk:128
tryingtobeanon:131
Vvs2:132
Mob!us:132
Fox_sibirsky:133
AlexKrsk:133
justlic:134
samely:135
mazafakawaka:136
Azork:138
Алекс812:138
Krymov Artyomka:138
freeExec:141
box_911:143
polemiker10:145
FvGordon:146
Premer:147
Александр Чуприна:150
alienchita:153
rmnturov:154
Владимир24041990:154
glebius:154
Andrey Astrakhanzev:155
Konstantin V Bekreyev:156
desk77:157
nick1973:159
mrJet:163
AnisKoutsi:163
s1eepwa1ker:163
Alexander-II:163
malcolmh:163
AnVas:165
aptala:166
iskitim79:169
Георгий Пепеляев:170
Hind:171
Vojtano:171
ivan-ivanych:171
rokuz:172
Serguei Dukachev:173
Fuse100A:177
СПТ Сочи:178
Marker13:179
Nicolai:181
Runich:181
Gassol:184
aleksey777:186
gorald:187
zuzubun:187
brlumen:187
Egor Kalinovsky:187
Andrey_E:190
_VAsek_:191
Пётр Попов:196
Dimcus:200
Lazarevsk:203
sebastic:210
doga:210
Andrej293:212
Siberia10:216
ewgeniy2004:220
Scaurr0ck:227
Федосов Сергей:229
quattroporte:232
iPhonin:232
Vladimir1990:232
Polyglot:232
dannykath:236
Ramir:240
keepright! ler:241
Пегий Пёс:245
Ogarasu:250
mkolomeitsev:254
Букин:257
Alexander Leschinsky:260
Xray2:266
andrean_:269
sefas:271
lifeslider:273
mesousa:273
Мещеряков Антон:273
Takdosh:274
alexz3:276
LexIgnatov:277
Денис Литвинов:279
Yuri Zimin:282
Maxim_NSK:283
Greensk:292
kir:295
UMA:298
rrv:298
dfcz55:298
Санчес:299
Taxiservis24:299
bayl:301
Stalker61:303
vezneves1:310
Karemater:311
dansit:314
Victor Kachesov:320
chilin:322
fin20:325
Alexey:328
yandobrov:331
LLlypuk82:333
PeLmEsHkA0:333
Aleksandr Dezhin:339
guggis:360
ogion76:363
Никита_Мисуль:364
alexmak:370
AndruXa9:379
aly0sha:392
полМаши:397
telefonist:405
DDD5:406
old_Bibigon:407
annaomhet:423
Pavel_Ku:424
olegta:425
orionll:425
tsvaan:425
chnav:430
YrslwW:438
shrk:450
dfcz55-73:455
Hunternsk:456
Angry Lizard:457
pale_blue_dot:460
veslav:474
Андрей Дунаев:476
Zhivin:480
parukhin:481
grozin:487
Eugene Muzychenko:489
uks:502
Mar Mar:508
Dmitriy Sedin:517
Nucleardragon:518
Павел2406:537
alex_iz_novosiba:543
54navi:548
trolleway:549
esaulenka:557
Silver87:559
telec121:566
Venograd:569
rogallic:577
stellz:580
Aleksey Ovchinnikov:581
flierfy:586
Golovco Anatolie:601
savage81:608
pacheng:618
denis_burij:620
Dyom:626
tanya2017:629
layt:629
maa:633
pankdm:639
Igor Reshta:649
Whitenight:659
Baris17:659
akbogdanov:660
Angara:665
KartaBY:678
FanCarver:699
donprogramist:715
devo4ka-napilnik:720
apLight:745
jmaks:745
dbf_:748
AMDmi3:748
Dommel5:753
mosstreet:763
ReiLester:764
Александр Обливальный:769
friedel_hn:793
Russian Killer:798
ts123plus:806
NikolaY:806
demon1981:813
langoor:822
Byrbl:828
kashcheeva:841
CoderD:849
Cronium:864
MD11:875
mochijanin:877
OldStar:888
axtom:897
Stane:906
progserega:913
kiceleff:925
Kosyak:933
test67:951
SolTomsk1:962
RussianMaus:983
Valeriy  Polunovskiy:992
Spol81:1026
amtor47:1026
Komяpa:1031
Slau:1079
Эцелоп:1115
МаксимГавронин:1118
Gorovato:1120
Xmypblu:1122
Yurez_nsk:1127
AleXanDeR_G:1170
Семён Семёнов:1179
ffk:1185
DmitrySA:1185
RSergei:1193
Inego:1211
bs1980:1214
Kostik:1248
IVM:1262
SiemensOFF:1266
Dmitry Y:1328
Vlad2011_2011:1350
koloboks:1362
pavel_vv:1379
dkiselev:1396
Виталий Махно:1571
OldIvantey:1668
lmaximov:1685
Brait:1825
anton_r54:1848
MichaelCollinson:1938
settler001:2028
zynsk:2116
Stroger:2219
filippov70:2433
Sasha_belok:2441
Upliner:2542
Айган:2546
Toliman:2566
Sergey N:2589
titanlis:2622
dimryb:2636
ghelius:2639
avi32:2687
gurin_andrey_vl:2774
Gde-ja:2888
kimol:2913
CrazyViny:2929
v-ozersk:2980
literan:3076
UncleS:3189
iWowik:3220
bekreyev:3283
olkhovatskikh:3324
orb04:3420
Gum4eg:3425
Sergey_Dzhagov:3470
alex_k1:3602
Artamonovserg:3743
GreedyGhost:3863
Anatoli:3931
Николай_К:3971
Владимир К:4017
Vodila_55:4102
Kuprienko_Viktor:4133
Fenuks:4148
Ошкуй:4528
ordorub:4633
Shtirlits:4638
#daf:4741
usama:5012
LeonidDemidow:5104
zebooka:5322
dndred:5365
max041:5467
PARKER1979:5542
Dharma-ae:5593
Takuto:5930
SunWind1076:6171
vanchester:6350
Андрей1978:6943
vanomel:7204
Denis Mack:7593
Starprinter:7689
te_mark:9832
andrewK:9873
malic:10037
ThaJk:10370
Karaban2008:10632
Natalia18:11344
serhod:12203
ImmortAlex:12716
toyotagps:13500
Z440:14374
pruosm:14505
katpatuka:15692
LGRV:16047
rockcreek:18565
Nikolaev:19874
afonin:19883
Павел Яринюк:20013
ilya3l:20701
MAS:21089
VlIvYur:23968
velonavt:24352
mike_lyulyukin:25786
maximaximax:26215
yohanson:27886
alex_hitman:28055
Magomogo:36468
Olegctt:41030
F1ex:45024
MrYadro:49705
Antonio_92:51906
ONT:53200
oo-O-oo:53957
Guamo:54010
michrom:55085
A54:58546
Anton Latkin:65704
demon_h:72548
koSS_Rus:79112
Ratty:80759
AndreySmirnov:88634
Sherper:89827
Kaylee:91341
Pavel Melnikov:94060
whalerich:102115
vitstud54:117087
Unken:127977
Jake Strine:138628
Басмач:318618
msergeyv100:414432
Moreorless:482782
siberiano:505341
Miroff:561444
xscvxc:2491869

Process finished with exit code 0

*/