package ru.nsu.mkirichenko.rs.task0;

import org.postgresql.jdbc.PgConnection;
import org.postgresql.util.HostSpec;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

/**
 * Created by mikhail on 06/03/2018, 14:43;
 */

@SpringBootApplication
public class Main implements CommandLineRunner {

	public static void main(String args[]) {
		SpringApplication.run(Main.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Thread.sleep(10000);
		HostSpec hostSpec[] = new HostSpec[1];
		hostSpec[0] = new HostSpec("postgres", 5432);
		String user = "docker";
		String database = "docker";
		Properties properties = new Properties();
		properties.setProperty("password", "docker");
		String url = "postgres";
		PgConnection conn = new PgConnection(hostSpec, user, database, properties, url);

		conn.setAutoCommit(false);
		Statement initialStatement = conn.createStatement();
		initialStatement.execute("DROP TABLE test_data");
		initialStatement.execute("CREATE TABLE test_data (id serial primary key, name text, cdate timestamp)");
		conn.commit();

		/*Test 1*/
		conn.createStatement().execute("TRUNCATE TABLE test_data");
		conn.commit();

		Statement statement = conn.createStatement();
		StringBuilder sqlBuilder = new StringBuilder(100);
		long beginTime = System.currentTimeMillis();
		for (int i = 0; i < 10000; ++i) {
			sqlBuilder.setLength(0);
			sqlBuilder.append("INSERT INTO test_data VALUES (");
			sqlBuilder.append(i);
			sqlBuilder.append(", 'Some text', '");
			sqlBuilder.append(new Timestamp(new Date().getTime()).toString());
			sqlBuilder.append("')");
			statement.execute(sqlBuilder.toString());
		}
		conn.commit();
		long totalTime = (System.currentTimeMillis() - beginTime);
		System.out.println("Test 1: " + 10000.0 * 1000.0 / (double)totalTime);

		/*Test 2*/
		conn.createStatement().execute("TRUNCATE TABLE test_data");
		conn.commit();

		PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO test_data VALUES (?, 'Some text', ?)");
		beginTime = System.currentTimeMillis();
		for (int i = 0; i < 10000; ++i) {
			preparedStatement.setInt(1, i);
			preparedStatement.setTimestamp(2, new Timestamp(new Date().getTime()));
			preparedStatement.execute();
		}
		conn.commit();
		totalTime = (System.currentTimeMillis() - beginTime);
		System.out.println("Test 2: " + 10000.0 * 1000.0 / (double)totalTime);

		/*Test 3*/
		conn.createStatement().execute("TRUNCATE TABLE test_data");
		conn.commit();

		PreparedStatement batchStatement = conn.prepareStatement("INSERT INTO test_data VALUES (?, 'Some text', ?)");
		beginTime = System.currentTimeMillis();
		for (int i = 0; i < 10000; ++i) {
			batchStatement.setInt(1, i);
			batchStatement.setTimestamp(2, new Timestamp(new Date().getTime()));
			batchStatement.addBatch();
		}
		batchStatement.executeBatch();
		conn.commit();
		totalTime = (System.currentTimeMillis() - beginTime);
		System.out.println("Test 3: " + 10000.0 * 1000.0 / (double)totalTime);
	}

}
